# ID4me Documentation

Following documentation is available for ID4me
* [General Overview](id4me%20General Overview%20v1.1.pdf) - Top level description
* [Technical Overview](id4me%20Technical%20Overview%20v1.2.2.pdf) - General protocol description on technical level
* [ID4me Technical Specification](id4ME%20Technical%20Specification.adoc) - Technical details of the protocol

## Lifecycle of the specification

* New version of the specification is published by new commits on master branch of this repository.
* New publication which change only the *Revision* number SHALL not break any of existing functionalities or integrations.
* Update to *Version* number means a breaking change and as such shall by all means be avoided.

## Issues and questions to the specification

Any issues, questions or feature requests to the specification can be raised by opening GitLab issue on this project.
All the discussion will be then held within the ticket.
In any case you can also reach out to Technical Competence Group: mailto:technical_wg@lists.ID4me.org

## Contribution

Anyone is encouraged to contribute to the specification and to propose changes or new functionalities.
The desired way of working is by forking the repository to a private account, make necessary changes and then issue a Pull Request back to the master.
The change will be then taken under Review by the Technical Competence Group of ID4me.

